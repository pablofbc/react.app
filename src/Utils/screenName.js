const restaurantStack = {
    tab: "RestaurantsTab",
};

const favoriteStack = {
    tab: "FavoritesTab",
};

const rankingStack = {
    tab: "RankingTab",
};

const searchStack = {
    tab: "SearchTab",
};

const accountStack = {
    tab: "AccountTab",
};


export const screen = {
    restaurant: restaurantStack,
    favorites: favoriteStack,
    ranking: rankingStack,
    search: searchStack,
    account: accountStack,

};